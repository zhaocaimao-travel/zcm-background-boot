package com.cykj.zcmbackgroundboot.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 小城南姝
 * @date 2023-12-27 11:13
 */
@Data
public class PassengerVO {
    private Integer passengerId;
    private String acc;
    private String name;
    private String phone;
    private String idCard;
    private String nickname;
    private String passengerImg;
    private String cardImg;
    private String familyAddress;
    private String companyAddress;
    private String sex;
    private String notes;
}

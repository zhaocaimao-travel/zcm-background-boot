package com.cykj.zcmbackgroundboot.vo;

import lombok.Data;

/**
 * @Author：自由辣
 * @Package：com.cykj.zcmbackgroundboot.vo
 * @Project：zcm-background-boot
 * @name：DriverTypeVO
 * @Date：2023/12/28 10:57
 * @Filename：DriverTypeVO
 */
//所有参数都用这个VO发到前端
@Data
public class ParamVO {
    private Integer paramId;
    private String name;
}

package com.cykj.zcmbackgroundboot.vo;


import com.cykj.zcmbackgroundboot.pojo.Process;
import lombok.Builder;
import lombok.Data;

/**
 * @author 小城南姝
 * @date 2023-12-28 20:16
 */
@Data
public class ProcessVO extends Process {
    private Integer paramId;
    private String paramName;
    private String phone;
    private String idCard;
}

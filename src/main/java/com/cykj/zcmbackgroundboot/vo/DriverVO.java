package com.cykj.zcmbackgroundboot.vo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DriverVO {

  private Integer driverId;
  private String acc;
  private String pwd;
  private String name;
  private String phone;
  private String idCard;
  private String cardImg;
  private String driverType;
  private String carName;
  private String carId;
  private String carModel;
  private String carColor;
  private String state;
  private Integer ratingStar;
  private String sex;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private Integer totalMoney;
  private Integer getMoney;
  private String notes;

}

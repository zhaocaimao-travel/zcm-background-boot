package com.cykj.zcmbackgroundboot.vo;

import com.cykj.zcmbackgroundboot.pojo.Orders;
import lombok.Data;

@Data
public class OrderVO extends Orders {

    private String driverName;
    private String passengerName;
}

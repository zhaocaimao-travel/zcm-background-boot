package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

@Data
public class Role {

  private Integer roleId;
  private String roleName;

}

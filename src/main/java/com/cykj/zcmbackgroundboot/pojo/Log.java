package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Log {
  private Integer logId;
  private LocalDateTime logTime;
  private String name;
  private String databaseName;
  private String operationContent;
  private String operationType;
  private Integer roleId;
  private String logMessage;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private Byte deleted;

}

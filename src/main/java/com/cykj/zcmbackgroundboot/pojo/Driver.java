package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Driver {

  private Integer driverId;
  private String acc;
  private String pwd;
  private String name;
  private String phone;
  private String idCard;
  private Integer age;
  private String jobId;
  private String cardImg;
  private String driverImg;
  private String carImg;
  private Integer driverType;
  private String carName;
  private String carId;
  private String carColor;
  private String state;
  private Integer ratingStar;
  private String sex;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private Integer totalMoney;
  private Integer getMoney;
  private String province;
  private String notes;
  private Byte deleted;
}

package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Route {

  private Integer pathId;
  private Long orderId;
  private BigDecimal longitude;
  private BigDecimal latitude;
  private double speed;
  private String direction;
  private String time;
}

package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Sound {

  private Integer soundId;
  private Long orderId;
  private LocalDateTime startTime;
  private LocalDateTime endTime;
  private Integer duration;
  private String soundPath;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private Byte deleted;

}

package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DriverRefuses {
  private Integer refuseId;
  private Long orderId;
  private Integer driverId;
  private LocalDateTime createTime;
  private String notes;
  private Byte deleted;

}

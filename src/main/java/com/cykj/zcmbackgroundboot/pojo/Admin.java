package com.cykj.zcmbackgroundboot.pojo;


import lombok.Data;

@Data
public class Admin {

  private Integer adminId;
  private String acc;
  private String pwd;
  private Integer check;
  private Integer roleId;
  private String notes;


}

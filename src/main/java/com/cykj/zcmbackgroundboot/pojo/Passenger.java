package com.cykj.zcmbackgroundboot.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@TableName("passenger")
public class Passenger {

  @TableId
  private Integer passengerId;
  private String acc;
  private String pwd;
  private String name;
  private String phone;
  private String idCard;
  private String nickname;
  private String passengerImg;
  private String cardImg;
  private String familyAddress;
  private String companyAddress;
  private String sex;
  private LocalDateTime createTime;
  @TableField(fill = FieldFill.UPDATE)
  private LocalDateTime updateTime;
  private String notes;
  private Byte deleted;

}

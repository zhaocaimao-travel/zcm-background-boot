package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Assess {

  private Integer assessId;
  private Long orderId;
  private double score;
  private String content;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private Integer deleted;

}

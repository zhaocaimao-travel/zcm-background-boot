package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Coupon {
  private Integer couponId;
  private Integer couponType;
  private Integer couponPoint;
  private Integer provideNum;
  private Integer useNum;
  private LocalDateTime startTime;
  private LocalDateTime overTime;
  private Integer status;
  private String notes;
  private Integer usePrice;

}

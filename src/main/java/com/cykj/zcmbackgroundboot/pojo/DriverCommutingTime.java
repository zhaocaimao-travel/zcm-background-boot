package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DriverCommutingTime {
  private Integer id;
  private Integer driverId;
  private LocalDateTime goWorkTime;
  private LocalDateTime offWorkTime;
  private Integer todayTime;
  private String notes;

}

package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Cars {

  private String plateId;
  private Integer carType;
  private Integer carColor;
  private String carName;
  private Integer driverId;
  private Byte seatNum;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private Byte deleted;

}

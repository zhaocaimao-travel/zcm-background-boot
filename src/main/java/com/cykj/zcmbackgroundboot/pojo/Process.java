package com.cykj.zcmbackgroundboot.pojo;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data

public class Process {

  private Integer processId;
  private Integer driverId;
  private Integer adminId;
  private String cardImg;
  private String driverType;
  private String carModel;
  private String carColor;
  private String carImg;
  private String carId;
  private String name;
  private Integer checkState;
  private String jobId;
  private String carName;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;

}

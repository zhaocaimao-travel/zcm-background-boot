package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

@Data
public class Param {

  private Integer paramId;
  private String name;
  private Integer pid;
  private String notes;

}

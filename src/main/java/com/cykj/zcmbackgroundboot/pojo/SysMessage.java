package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysMessage {

  private Integer sysMessageId;
  private Integer receiverType;
  private Integer messageType;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private Integer state;
  private String notes;
  private String messageContent;
  private Byte deleted;
}

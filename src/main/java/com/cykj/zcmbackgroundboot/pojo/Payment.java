package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Payment {

  private Integer paymentId;
  private Long orderId;
  private Integer price;
  private LocalDateTime payTime;
  private Integer state;
  private Integer coupon;
  private Integer payType;
  private String notes;
  private Byte deleted;

}

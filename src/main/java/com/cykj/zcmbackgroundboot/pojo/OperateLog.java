package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OperateLog {

  private Integer id;
  private String module;
  private String description;
  private String requestMethod;
  private String operateMethod;
  private String user;
  private String operateIp;
  private String operateSource;
  private long status;
  private String statusDescription;
  private String requestParam;
  private long duration;
  private LocalDateTime operateTime;

}

package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

@Data
public class Menu {

  private Integer menuId;
  private String menuName;
  private String path;
  private Integer pid;

}

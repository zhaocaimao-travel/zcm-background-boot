package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

@Data
public class PassengerCoupon {

  private Integer id;
  private Integer passengerId;
  private Integer couponId;


}

package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class HistoryOrders {

  private Long orderId;
  private Integer passengerId;
  private Integer driverId;
  private String startAddress;
  private String endAddress;
  private Integer carType;
  private Integer state;
  private LocalDateTime startTime;
  private LocalDateTime endTime;
  private Integer duration;
  private Integer distance;
  private Integer startPrice;
  private Integer mileagePrice;
  private Integer durationPrice;
  private String cancelReason;
  private BigDecimal startLongitude;
  private BigDecimal endLongitude;
  private BigDecimal startLatitude;
  private BigDecimal endLatitude;
  private Integer totalPrice;
  private Integer waitPrice;
  private Integer upPrice;
  private Integer expectTime;
  private Integer realityTime;
  private Integer expectPrice;
  private Integer realityPrice;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private Byte deleted;

}

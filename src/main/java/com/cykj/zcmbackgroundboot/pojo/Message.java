package com.cykj.zcmbackgroundboot.pojo;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Message {

  private Integer messageId;
  private Integer senderId;
  private String senderType;
  private Integer receiverId;
  private String receiverType;
  private Integer content;
  private LocalDateTime sendTime;
  private Integer state;
  private Integer type;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private String messageContent;
  private Byte deleted;

}

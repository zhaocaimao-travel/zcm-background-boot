package com.cykj.zcmbackgroundboot.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Complaint {

  private Integer complaintId;
  private Integer driverId;
  private String content;
  private Integer state;
  private LocalDateTime createTime;
  private LocalDateTime updateTime;
  private String notes;
  private Byte deleted;


}

package com.cykj.zcmbackgroundboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement//启用事物管理
public class ZcmBackgroundBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZcmBackgroundBootApplication.class, args);
    }

}

package com.cykj.zcmbackgroundboot.result;

import lombok.Getter;

@Getter
public enum ResultCode{
    SUCCESS(200, "请求成功"),
    GET_SUCCESS(1, "获取数据成功"),
    ERROR(0, "账号或密码错误"),
    APP_ERROR(2000, "业务异常"),
    EXCEPTION_ERROR(5000,"系统内部错误，请联系管理员");

    private int code;
    private String msg;
    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

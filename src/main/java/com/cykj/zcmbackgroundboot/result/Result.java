package com.cykj.zcmbackgroundboot.result;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    /**
     * 状态码
     */
    private int code;
    /**
     * 状态信息
     */
    private String msg;
    /**
     * 返回数据
     */
    private T data;

    /**
     * 是否成功
     */

    public static <T> Result<T> error(ResultCode statusCode) {
        return new Result<T>(statusCode.getCode(), statusCode.getMsg(), null);
    }

    public static <T> Result<T> error(ResultCode statusCode, T data) {
        return new Result<T>(statusCode.getCode(), statusCode.getMsg(), data);
    }

    public static <T> Result<T> success(ResultCode statusCode, T data) {
        return new Result<T>(statusCode.getCode(), statusCode.getMsg(), data);
    }

    public static <T> Result<T> success(T data) {
        return new Result<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), data);
    }


}

package com.cykj.zcmbackgroundboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.DriverDTO;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.result.ResultCode;
import com.cykj.zcmbackgroundboot.service.DriverService;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/driver")
@RestController
public class DriverController {

    @Autowired
    private DriverService driverService;

    /**
     * 获取乘客信息
     */
    @GetMapping("/getDrivers")
    public Result getDrivers(int pageNum,int pageSize){
        Page<Driver> driverInfo = driverService.getDriverInfo( pageNum, pageSize);
        return Result.success(driverInfo);
    }
//按条件查司机信息
    @GetMapping("/searchDriver")
    public Result searchDrivers(@ModelAttribute DriverDTO driverDTO){
        Page<Driver> driverInfo = driverService.searchDriverInfo(driverDTO);
        return Result.success(driverInfo);
    }
//获取司机类型
    @GetMapping("/getDriverType")
    public Result getDriverType(){
        List<ParamVO> driverType = driverService.getDriverType();
        return Result.success(driverType);
    }
//假删除司机信息
    @PutMapping("/deleteByDriverId")
    public Result delete(int driverId){
        int deleteByDriverId = driverService.deleteByDriverId(driverId);
        if(deleteByDriverId>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }

    //批量删除司机信息-假删除
    @PutMapping("/batchDeleteDriver")
    public Result delete(@RequestBody List<Integer> driverList){
        driverList.forEach(System.err::println);
        System.out.println(driverList);
        int i = driverService.batchDeleteDriver(driverList);
        if(i>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }
//修改司机信息
    @PutMapping("/updataDriverInfo")
    public Result updataDriverInfo(Driver driver){
        int i = driverService.updataDriverInfo(driver);
        if(i>0){
            return Result.success("修改成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }
    //重置密码
    @PutMapping("/resetPwd")
    public Result resetPwd(int driverId){
        int resetPwd = driverService.resetPwd(driverId);
        if(resetPwd>0){
            return Result.success("重置密码成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }
}

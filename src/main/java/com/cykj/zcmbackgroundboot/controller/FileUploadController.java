package com.cykj.zcmbackgroundboot.controller;

import com.cykj.zcmbackgroundboot.exception.CustomException;
import com.cykj.zcmbackgroundboot.util.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author 小城南姝
 * @date 2023-12-28 15:05
 */
@RestController
public class FileUploadController {
    @Autowired
    private OssUtil ossUtil;
    @PostMapping("/uploadPhoto")
    public String uploadPhoto(@RequestParam("file") MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            String fileName = file.getOriginalFilename();
            String location = "photos"; // 设置文件上传的位置，例如 photos 文件夹下
            String photoUrl = ossUtil.upload(location, fileName, inputStream);
            return photoUrl;
        } catch (IOException e) {
            throw CustomException.transform("照片上传异常");
        }
    }
}

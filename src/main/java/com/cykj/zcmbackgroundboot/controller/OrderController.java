package com.cykj.zcmbackgroundboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.OrderDTO;
import com.cykj.zcmbackgroundboot.pojo.Orders;
import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.result.ResultCode;
import com.cykj.zcmbackgroundboot.service.OrderService;
import com.cykj.zcmbackgroundboot.vo.OrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/order")
@RestController
@Slf4j
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 获取订单信息 - 分页方式
     *
     * @param pageNum  当前页
     * @param pageSize 每页大小
     * @return 订单信息
     */
    @GetMapping("/getOrders")
    public Result getOrders(int pageNum, int pageSize) {
        return Result.success(orderService.getOrderInfo(pageNum, pageSize));
    }

    /**
     * 搜索订单信息并分页
     * @param orderDTO  存放搜索条件，当前页，每页大小
     * @return  搜索后的订单消息
     */
    @GetMapping("/getOrdersByCondition")
    public Result getOrders(@ModelAttribute OrderDTO orderDTO) {
        log.error(orderDTO.toString());
        Page<OrderVO> ordersPage = orderService.searchOrderVO(orderDTO);
        return Result.success(ordersPage);
    }

    /**
     * 删除订单信息 - 假删除
     * @param orderId   订单id
     * @return  受影响的数据行数
     */
    @PutMapping("/deleteByOrderId")
    public Result delete(int orderId){
        int i = orderService.deleteByOrderId(orderId);
        if(i>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }

    /**
     * 批量删除订单信息
     * @param list  存放订单id的数组
     * @return  受影响的数据行数
     */
    @PutMapping("/batchDeleteOrders")
    public Result delete(@RequestBody List<Integer> list){
        list.forEach(System.err::println);
        System.out.println(list);
        int i = orderService.batchDeleteOrder(list);
        if(i>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }
}

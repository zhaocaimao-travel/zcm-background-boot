package com.cykj.zcmbackgroundboot.controller;

import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/payment")
@RestController
@Slf4j
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @GetMapping("/payments")
    public Result getPayments(int pageNum, int pageSize){
        return Result.success(paymentService.getPaymentInfo(pageNum,pageSize));
    }
}

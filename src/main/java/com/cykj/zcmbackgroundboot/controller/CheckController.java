package com.cykj.zcmbackgroundboot.controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.CheckDTO;
import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.result.ResultCode;
import com.cykj.zcmbackgroundboot.service.CheckService;
import com.cykj.zcmbackgroundboot.vo.ProcessVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 小城南姝
 * @date 2023-12-28 19:42
 */
@RestController()
@RequestMapping("/check")
@Slf4j
public class CheckController {

    @Autowired
    private CheckService checkService;

    /**
     * 获取司机申请消息
     * @param checkDTO 带分页查询消息，包括姓名，申请车型及申请状态
     * @return 返回审核列表
     */
    @GetMapping("/checkDriveInfo")
    public Result checkDriverInfo(@ModelAttribute CheckDTO checkDTO){

        Page<ProcessVO> page = checkService.checkDriverInfo(checkDTO);
        return Result.success(page);
    }

    /**
     * 同意审核消息，修改状态，
     * @param checkDTO senderId;//发送者id
     * @return 是否同意成功
     */
    @PostMapping("agreeCheck")
    public Result agreeCheck(@RequestBody CheckDTO checkDTO){
        int i = checkService.agreeCheck(checkDTO);
        if (i > 0) {
            return Result.success("审核通过");
        } else {
            return Result.error(ResultCode.APP_ERROR);
        }
    }

    /**
     * 拒绝审核消息
     * @param checkDTO senderId: 拒绝者id
     *           reason: 拒绝原因
     *           driverId: 拒绝司机id
     *           processId: 拒绝的id
     *           createTime: 拒绝申请的时间
     *           name: 申请者姓名
     * @return 是否拒绝成功
     */
    @PostMapping("refuseCheck")
    public Result refuseCheck(@RequestBody CheckDTO checkDTO){
        System.err.println(checkDTO.toString());
        int i = checkService.refuseCheck(checkDTO);
        if (i > 0) {
            return Result.success("拒绝成功");
        } else {
            return Result.error(ResultCode.APP_ERROR);
        }
    }
}

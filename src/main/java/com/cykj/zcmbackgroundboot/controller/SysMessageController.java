package com.cykj.zcmbackgroundboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.DriverDTO;
import com.cykj.zcmbackgroundboot.dto.PassengerDTO;
import com.cykj.zcmbackgroundboot.dto.SysMessageDTO;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.pojo.SysMessage;
import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.result.ResultCode;
import com.cykj.zcmbackgroundboot.service.DriverService;
import com.cykj.zcmbackgroundboot.service.SysMessageService;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：自由辣
 * @Package：com.cykj.zcmbackgroundboot.controller
 * @Project：zcm-background-boot
 * @name：SysMessageController
 * @Date：2023/12/28 17:56
 * @Filename：SysMessageController
 */

@RequestMapping("/sysMessage")
@RestController
public class SysMessageController {
    @Autowired
    private SysMessageService sysMessageService;

    @GetMapping("/getSysMessage")
    public Result getSysMessage(int pageNum, int pageSize){
        Page<SysMessage> sysMessageInfo = sysMessageService.getSysMessageInfo(pageNum, pageSize);
        return Result.success(sysMessageInfo);
    }

    //按条件查询系统消息
    @GetMapping("/searchSysMessage")
    public Result searchSysMessage(@ModelAttribute SysMessageDTO sysMessageDTO){
        Page<SysMessage> sysMessagePage = sysMessageService.searchSysMessageInfo(sysMessageDTO);
        return Result.success(sysMessagePage);
    }

    //添加系统消息
    @PostMapping("addSysMessage")
    public Result addSysMessage(@RequestBody SysMessageDTO sysMessageDTO){
        int i = sysMessageService.addSysMessage(sysMessageDTO);
        return Result.success(i);

    }

    //获取接受者类型
    @GetMapping("/getReceiverType")
    public Result getReceiverType(){
        List<ParamVO> receiverType = sysMessageService.getReceiverType();
        return Result.success(receiverType);
    }

    //获取系统消息状态
    @GetMapping("/getSysMessageState")
    public Result getSysMessageState(){
        List<ParamVO> sysMessageState = sysMessageService.getSysMessageState();
        return Result.success(sysMessageState);
    }

    //获取系统消息类型
    @GetMapping("/getSysMessageType")
    public Result getSysMessageType(){
        List<ParamVO> sysMessageType = sysMessageService.getSysMessageType();
        return Result.success(sysMessageType);
    }

    //假删除系统信息
    @PutMapping("/deleteSysMessage")
    public Result deleteSysMessage(int sysMessageId){
        int i = sysMessageService.deleteSysMessage(sysMessageId);
        if(i>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }

    //批量删除系统信息-假删除
    @PutMapping("/batchDeleteSysMessage")
    public Result delete(@RequestBody List<Integer> sysMessageIdList){
        sysMessageIdList.forEach(System.err::println);
        System.out.println(sysMessageIdList);
        int batchDeleteSysMessage = sysMessageService.batchDeleteSysMessage(sysMessageIdList);
        if(batchDeleteSysMessage>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }

}

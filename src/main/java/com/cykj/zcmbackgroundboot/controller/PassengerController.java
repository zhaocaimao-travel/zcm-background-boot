package com.cykj.zcmbackgroundboot.controller;

import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.PassengerDTO;
import com.cykj.zcmbackgroundboot.pojo.Passenger;
import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.result.ResultCode;
import com.cykj.zcmbackgroundboot.service.PassengerService;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 小城南姝
 * @date 2023-12-26 15:25
 */
@RestController
@RequestMapping("/passenger")
@Slf4j
public class PassengerController {

    @Autowired
    private PassengerService passengerService;

    /**
     * 分页查询
     * name用户名
     * phone手机号
     * idCard身份证号
     * @param query
     * @return
     */
    @GetMapping("/passengerInfo")
    public Result getPassengerInfo(@ModelAttribute PassengerDTO query){
        Page<Passenger> passengerInfo = passengerService.getPassengerInfo(query);
        return Result.success(passengerInfo);
    }

    /**
     * 假删除
     * @param乘客id数组
     * @return
     */
    @PutMapping("/passengerInfo")
    public Result deleteByInfoId(@RequestBody List<Integer> passengerIdArr){
        int i = passengerService.deleteByInfoId(passengerIdArr);
        if(i>0){
            return Result.success("删除成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }

    /**
     * 修改密码
     * @param passengerId 用户id
     * @return
     */
    @PutMapping("resetPassengerPwd")
    public Result resetPassengerPwd(int passengerId){
        int i = passengerService.resetPassengerPwd(passengerId);
        if(i>0){
            return Result.success("重置成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }

    @PostMapping("editPassengerInfo")
    public Result editPassengerInfo(@RequestBody PassengerDTO passengerDTO){
        int i = passengerService.editPassengerInfo(passengerDTO);
        if(i>0){
            return Result.success("修改成功");
        }else{
            return Result.error(ResultCode.APP_ERROR);
        }
    }
}

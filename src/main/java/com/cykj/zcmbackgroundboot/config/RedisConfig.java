package com.cykj.zcmbackgroundboot.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author 小宇sir
 * @Description //TODO
 * @Date 2023/12/3 03:03
 * redis序列化和反序列化的配置类
 **/
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {

    //Redis 的键序列化器，用于将键对象序列化成字符串
    private final StringRedisSerializer kaySerializer = new StringRedisSerializer();
    //值序列化器，使用 Jackson 库将对象序列化成 JSON 字符串
    private final GenericJackson2JsonRedisSerializer valueSerializer = new GenericJackson2JsonRedisSerializer();

    @Bean(name = "redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        //参照StringRedisTemplate内部实现指定序列化器
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(kaySerializer);
        redisTemplate.setValueSerializer(valueSerializer);
        redisTemplate.setHashKeySerializer(kaySerializer);
        redisTemplate.setHashValueSerializer(valueSerializer);
        return redisTemplate;
    }

    /**
     * 设置redis过期时间
     *
     * @param redisConnectionFactory user是登录的过期时间另一个可以在service的实现使用默认时间是1小时
     * @return
     */
    @Bean
    public CacheManager customCacheManager(RedisConnectionFactory redisConnectionFactory) {
        RedisCacheConfiguration userCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofMinutes(5)); // 设置users缓存的过期时间为5分钟

        RedisCacheConfiguration productCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofHours(1)); // 设置products缓存的过期时间为1小时

        Map<String, RedisCacheConfiguration> cacheConfigurations = new HashMap<>();
        cacheConfigurations.put("user", userCacheConfiguration);
        cacheConfigurations.put("products", productCacheConfiguration);

        return RedisCacheManager.builder(redisConnectionFactory)
                .withInitialCacheConfigurations(cacheConfigurations)
                .cacheDefaults(userCacheConfiguration) // 默认缓存配置
                .build();
    }
}

//    private RedisSerializer<String> keySerializer(){
//        return new StringRedisSerializer();
//    }
//
//    /**
//     * 使用Jackson序列化器
//     * @return
//     */
//    private RedisSerializer<Object> valueSerializer(){
//        return new GenericJackson2JsonRedisSerializer();
//    }

package com.cykj.zcmbackgroundboot.dto;

import lombok.Data;
import org.apache.catalina.authenticator.SavedRequest;

@Data
public class CheckDTO {
    public Integer processId;
    private String name;
    private Integer driverType;//司机类型
    private String carColor;//车辆颜色
    private String carId;
    private String jobId;
    private String carName;
    private String carImg;
    private String cardImg;
    private String checkState;
    private int currentPage;
    private Integer pageSize;

    private Integer senderId;//发送者id
    private Integer driverId;//接受者id
    private String message;//消息
    private String createTime;//申请时间
    private String reason;//拒绝原因




}

package com.cykj.zcmbackgroundboot.dto;

import lombok.Data;

@Data
public class DriverDTO {
    private Integer driverId;
    private String driverName;
    private String jobId;
    private Integer pageNum;
    private Integer pageSize;
}

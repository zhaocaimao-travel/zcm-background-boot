package com.cykj.zcmbackgroundboot.dto;

import lombok.Data;

/**
 * @Author：自由辣
 * @Package：com.cykj.zcmbackgroundboot.dto
 * @Project：zcm-background-boot
 * @name：SysMessageDTO
 * @Date：2023/12/28 21:37
 * @Filename：SysMessageDTO
 */
@Data
public class SysMessageDTO {
    private Integer pageNum;
    private Integer pageSize;
    private Integer receiverType;
    private Integer messageType;
    private Integer state;
    private String messageContent;
    private String notes;
}

package com.cykj.zcmbackgroundboot.dto;

import lombok.Data;

/**
 * @author 小城南姝
 * @date 2023-12-27 9:29
 */
@Data
public class PassengerDTO {
    public Integer passengerId;
    private String name;
    private String phone;
    private String idCard;
    private int currentPage;
    private Integer pageSize;
    private String nickname;
    private  String companyAddress;

}

package com.cykj.zcmbackgroundboot.dto;

import lombok.Data;

@Data
public class OrderDTO {
    private Integer driverId;
    private Integer passengerId;
    private String driverName;
    private String passengerName;
    private int pageNum;
    private int pageSize;
}

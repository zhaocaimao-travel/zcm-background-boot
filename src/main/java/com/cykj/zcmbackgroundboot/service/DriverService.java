package com.cykj.zcmbackgroundboot.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cykj.zcmbackgroundboot.dto.DriverDTO;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DriverService extends IService<Driver> {
    Page<Driver> getDriverInfo(int pageNum,int pageSize);
    Page<Driver> searchDriverInfo(DriverDTO driverDTO);
    List<ParamVO> getDriverType();

    int deleteByDriverId(int driverId);
    int resetPwd(int driverId);
    int updataDriverInfo(Driver driver);//修改司机信息
    int batchDeleteDriver(List<Integer> driverList);
}

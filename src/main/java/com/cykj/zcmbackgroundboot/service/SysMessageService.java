package com.cykj.zcmbackgroundboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cykj.zcmbackgroundboot.dto.DriverDTO;
import com.cykj.zcmbackgroundboot.dto.SysMessageDTO;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.pojo.SysMessage;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author：自由辣
 * @Package：com.cykj.zcmbackgroundboot.service
 * @Project：zcm-background-boot
 * @name：SysMessageSerice
 * @Date：2023/12/28 17:58
 * @Filename：SysMessageSerice
 */
@Service
public interface SysMessageService extends IService<SysMessage> {
    Page<SysMessage> getSysMessageInfo(int pageNum, int pageSize);
    Page<SysMessage> searchSysMessageInfo(SysMessageDTO sysMessageDTO);
    List<ParamVO> getReceiverType();
    List<ParamVO> getSysMessageState();
    List<ParamVO> getSysMessageType();
    int addSysMessage(SysMessageDTO sysMessageDTO);
    int deleteSysMessage(@Param("sysMessageId") int sysMessageId);
    int batchDeleteSysMessage(@Param("sysMessageList")List<Integer> list);
}

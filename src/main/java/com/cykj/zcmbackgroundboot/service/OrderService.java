package com.cykj.zcmbackgroundboot.service;

import com.cykj.zcmbackgroundboot.dto.OrderDTO;
import com.cykj.zcmbackgroundboot.pojo.Orders;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.vo.OrderVO;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {
    /**
     * 获取订单信息 - 分页方式
     * @param pageNum   当前页
     * @param pageSize  每页大小
     * @return  订单信息
     */
//    Page<Orders> getOrders(int pageNum,int pageSize);

    /**
     * 搜索订单信息并分页
     * @param orderDTO  存放搜索条件，当前页，每页大小
     * @return  搜索后的订单消息
     */
//    Page<Orders> searchOrders(OrderDTO orderDTO);

    /**
     * 删除订单信息 - 假删除
     * @param orderId   订单id
     * @return  受影响的数据行数
     */
    int deleteByOrderId(int orderId);

    /**
     * 批量删除订单信息 - 假删除
     * @param list  存放订单id的数组
     * @return  受影响的数据行数
     */
    int batchDeleteOrder(List<Integer> list);

    /**
     * 获取订单信息 - 分页方式
     * @param pageNum   当前页
     * @param pageSize  每页大小
     * @return  订单信息
     */
    Page<OrderVO> getOrderInfo(int pageNum,int pageSize);

    /**
     * 搜索订单信息并分页
     * @param orderDTO  存放搜索条件，当前页，每页大小
     * @return  搜索后的订单消息
     */
    Page<OrderVO> searchOrderVO(OrderDTO orderDTO);

}

package com.cykj.zcmbackgroundboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cykj.zcmbackgroundboot.dto.CheckDTO;
import com.cykj.zcmbackgroundboot.dto.PassengerDTO;
import com.cykj.zcmbackgroundboot.pojo.Passenger;
import com.cykj.zcmbackgroundboot.vo.ProcessVO;

import java.util.List;

/**
 * @author 小城南姝
 * @date 2023-12-26 15:29
 */
public interface CheckService{
    Page<ProcessVO> checkDriverInfo(CheckDTO checkDTO);

    int agreeCheck(CheckDTO checkDTO);

    int refuseCheck(CheckDTO checkDTO);


}

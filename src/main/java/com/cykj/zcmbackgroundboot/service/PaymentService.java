package com.cykj.zcmbackgroundboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.pojo.Payment;
import org.springframework.stereotype.Service;

@Service
public interface PaymentService {
    Page<Payment> getPaymentInfo(int pageNum,int pageSize);
}

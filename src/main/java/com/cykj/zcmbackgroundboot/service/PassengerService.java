package com.cykj.zcmbackgroundboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cykj.zcmbackgroundboot.dto.PassengerDTO;
import com.cykj.zcmbackgroundboot.pojo.Passenger;
import com.cykj.zcmbackgroundboot.result.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 小城南姝
 * @date 2023-12-26 15:29
 */
public interface PassengerService extends IService<Passenger> {

    Page<Passenger> getPassengerInfo(PassengerDTO passengerDTO);

    int deleteByInfoId(List<Integer> passengerId);

    int resetPassengerPwd(int passengerId);

    int editPassengerInfo(PassengerDTO passengerDTO);
}

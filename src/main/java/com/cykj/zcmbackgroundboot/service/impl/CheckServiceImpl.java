package com.cykj.zcmbackgroundboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.CheckDTO;
import com.cykj.zcmbackgroundboot.mapper.CheckMapper;
import com.cykj.zcmbackgroundboot.service.CheckService;
import com.cykj.zcmbackgroundboot.util.MessageUtil;
import com.cykj.zcmbackgroundboot.vo.ProcessVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小城南姝
 * @date 2023-12-26 15:29
 */
@Service
public class CheckServiceImpl implements CheckService {

    @Autowired
    private CheckMapper checkMapper;


    /**
     * 获取司机申请信息
     * @param checkDTO 动态分页查询字段
     * @return 返回司机申请信息
     */
    @Override
    public Page<ProcessVO> checkDriverInfo(CheckDTO checkDTO) {
        Page<ProcessVO> page = new Page<>(checkDTO.getCurrentPage(),checkDTO.getPageSize());
        QueryWrapper<ProcessVO> wrapper = new QueryWrapper<>();
        if(StringUtils.checkValNotNull(checkDTO.getName())){
            wrapper.like("t1.name",checkDTO.getName());
        }
        if(StringUtils.checkValNotNull(checkDTO.getDriverType())){
            wrapper.eq("t1.driver_type",checkDTO.getDriverType());
        }
        if(StringUtils.checkValNotNull(checkDTO.getCheckState())){
            wrapper.eq("t1.check_state",checkDTO.getCheckState());
        }
        System.err.println(wrapper.getCustomSqlSegment());
        Page<ProcessVO> orderInfo = checkMapper.checkDriverInfo(page,wrapper);
        return orderInfo;
    }

    /**
     * 同意申请
     * 修改申请状态，向信息表插入信息，向司机表插入信息
     * @param checkDTO
     * @return
     */
    @Transactional
    @Override
    public int agreeCheck(CheckDTO checkDTO) {
        int isUpdate = checkMapper.upDateCheckState(checkDTO.processId);
        String currTime = MessageUtil.getCurrTime();
        String message = MessageUtil.agreeApplyMessage(checkDTO.getName(), checkDTO.getCreateTime(), currTime);
        checkDTO.setMessage(message);
        int isInsert = checkMapper.insertMessage(checkDTO);
        int isDriver = checkMapper.insertDriver(checkDTO);
        if (isUpdate==1 && isInsert==1 && isDriver==1){
            return 2;
        }else {
            return -1;
        }
    }

    @Transactional
    @Override
    public int refuseCheck(CheckDTO checkDTO) {
        int isRefuse = checkMapper.updateRefuseCheckState(checkDTO.processId);
        String currTime = MessageUtil.getCurrTime();
        String message = MessageUtil.rejectApplyMessage(checkDTO.getName(), checkDTO.getCreateTime(), currTime, checkDTO.getReason());
        checkDTO.setMessage(message);
        int isInsert = checkMapper.insertMessage(checkDTO);
        if (isRefuse==1 && isInsert==1){
            return 2;
        }else {
            return -1;
        }
    }
}

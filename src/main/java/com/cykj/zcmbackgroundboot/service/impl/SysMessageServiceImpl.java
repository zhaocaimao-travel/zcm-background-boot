package com.cykj.zcmbackgroundboot.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cykj.zcmbackgroundboot.dto.SysMessageDTO;
import com.cykj.zcmbackgroundboot.mapper.SysMessageMapper;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.pojo.SysMessage;
import com.cykj.zcmbackgroundboot.service.SysMessageService;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author：自由辣
 * @Package：com.cykj.zcmbackgroundboot.service.impl
 * @Project：zcm-background-boot
 * @name：SysMessageImpl
 * @Date：2023/12/28 18:00
 * @Filename：SysMessageImpl
 */
@Service
public class SysMessageServiceImpl extends ServiceImpl<SysMessageMapper, SysMessage> implements SysMessageService {

    @Autowired
    private SysMessageMapper sysMessageMapper;

    @Override
    public Page<SysMessage> getSysMessageInfo(int pageNum, int pageSize) {

        // 创建一个 QueryWrapper 对象
        QueryWrapper<SysMessage> queryWrapper = new QueryWrapper<>();
        // 或者使用 selectPage 方法进行分页查询
        Page<SysMessage> page = new Page<>( pageNum, pageSize);  // 按当前页和每页条数查询记录
        sysMessageMapper.selectPage(page, queryWrapper);
        return page;
    }
    @Override
    public Page<SysMessage> searchSysMessageInfo(SysMessageDTO sysMessageDTO) {
        // 创建一个 QueryWrapper 对象
        QueryWrapper<SysMessage> queryWrapper = new QueryWrapper<>();
        if(StringUtils.checkValNotNull(sysMessageDTO.getMessageType())){
            queryWrapper.like("message_type",sysMessageDTO.getMessageType());
        }
        if(StringUtils.checkValNotNull(sysMessageDTO.getReceiverType())){
            queryWrapper.like("receiver_type",sysMessageDTO.getReceiverType());
        }
        if(StringUtils.checkValNotNull(sysMessageDTO.getState())){
            queryWrapper.like("state",sysMessageDTO.getState());
        }
        if(StringUtils.isNotBlank(sysMessageDTO.getMessageContent())){
            queryWrapper.like("message_content",sysMessageDTO.getMessageContent());
        }

        // 或者使用 selectPage 方法进行分页查询
        Page<SysMessage> page = new Page<>(sysMessageDTO.getPageNum(), sysMessageDTO.getPageSize());  // 按当前页和每页条数查询记录
        sysMessageMapper.selectPage(page, queryWrapper);
        return page;
    }
    @Override
    public List<ParamVO> getReceiverType() {
        return sysMessageMapper.getReceiverType();
    }

    @Override
    public List<ParamVO> getSysMessageState() {
        return sysMessageMapper.getSysMessageState();
    }

    @Override
    public List<ParamVO> getSysMessageType() {
        return sysMessageMapper.getSysMessageType();
    }

    @Override
    public int addSysMessage(SysMessageDTO sysMessageDTO) {
        if (sysMessageDTO.getReceiverType() == -1) {
            sysMessageDTO.setReceiverType(47);
            sysMessageMapper.addSysMessage(sysMessageDTO);
            sysMessageDTO.setReceiverType(48);
            sysMessageMapper.addSysMessage(sysMessageDTO);
        }
        
        return sysMessageMapper.addSysMessage(sysMessageDTO);
    }

    @Override
    public int deleteSysMessage(int sysMessageId) {
        return sysMessageMapper.deleteSysMessage(sysMessageId);
    }

    @Override
    public int batchDeleteSysMessage(List<Integer> list) {
        return sysMessageMapper.batchDeleteSysMessage(list);
    }


}

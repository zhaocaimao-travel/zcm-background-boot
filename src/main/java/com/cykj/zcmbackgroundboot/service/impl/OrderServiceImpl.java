package com.cykj.zcmbackgroundboot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.OrderDTO;
import com.cykj.zcmbackgroundboot.mapper.OrderMapper;
import com.cykj.zcmbackgroundboot.pojo.Orders;
import com.cykj.zcmbackgroundboot.service.OrderService;
import com.cykj.zcmbackgroundboot.vo.OrderVO;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 获取订单信息 - 分页方式
     * @param pageNum   当前页
     * @param pageSize  每页大小
     * @return  订单信息
     */
//    @Override
//    public Page<Orders> getOrders(int pageNum,int pageSize) {
//        // 创建一个QueryWrapper对象
//        QueryWrapper<Orders> queryWrapper = new QueryWrapper<>();
//        // 或者使用 selectPage 方法进行分页查询
//        Page<Orders> page = new Page<>(pageNum,pageSize);
//        orderMapper.selectPage(page,queryWrapper);
//        return page;
//    }

    /**
     * 搜索订单信息并分页
     * @param orderDTO  存放搜索条件，当前页，每页大小
     * @return  搜索后的订单消息
     */
//    @Override
//    public Page<Orders> searchOrders(OrderDTO orderDTO) {
//        // 创建一个QueryWrapper对象
//        QueryWrapper<Orders> queryWrapper = new QueryWrapper<>();
//        if(StringUtils.checkValNotNull(orderDTO.getDriverId())){
//            queryWrapper.like("driver_id",orderDTO.getDriverId());
//        }
//        if(StringUtils.checkValNotNull(orderDTO.getPassengerId())){
//            queryWrapper.like("passenger_id",orderDTO.getPassengerId());
//        }
//        // 或者使用 selectPage 方法进行分页查询
//        Page<Orders> page = new Page<>(orderDTO.getPageNum(),orderDTO.getPageSize());
//        orderMapper.selectPage(page,queryWrapper);
//        return page;
//    }

    /**
     * 搜索订单信息并分页
     * @param orderDTO  存放搜索条件，当前页，每页大小
     * @return  搜索后的订单消息
     */
    @Override
    public Page<OrderVO> searchOrderVO(OrderDTO orderDTO) {
        // 创建一个QueryWrapper对象
        QueryWrapper<OrderVO> queryWrapper = new QueryWrapper<>();
        if(StringUtils.checkValNotNull(orderDTO.getDriverName())){
            queryWrapper.like("t2.name",orderDTO.getDriverName());
        }
        if(StringUtils.checkValNotNull(orderDTO.getPassengerName())){
            queryWrapper.like("t3.name",orderDTO.getPassengerName());
        }
        // 或者使用 selectPage 方法进行分页查询
        Page<OrderVO> page = new Page<>(orderDTO.getPageNum(),orderDTO.getPageSize());
        orderMapper.getOrderInfo(page,queryWrapper);
        return page;
    }

    /**
     * 删除订单信息 - 假删除
     * @param orderId   订单id
     * @return  受影响的数据行数
     */
    @Override
    public int deleteByOrderId(int orderId) {
        return orderMapper.deleteByOrderIdInt(orderId);
    }

    /**
     * 批量删除订单信息 - 假删除
     * @param list  存放订单id的数组
     * @return  受影响的数据行数
     */
    @Override
    public int batchDeleteOrder(List<Integer> list) {
        return orderMapper.batchDeleteOrder(list);
    }


    /**
     * 获取订单信息 - 分页方式
     * @param pageNum   当前页
     * @param pageSize  每页大小
     * @return  订单信息
     */
    @Override
    public Page<OrderVO> getOrderInfo(int pageNum,int pageSize) {
        Page<OrderVO> page = new Page<>(pageNum, pageSize);
        QueryWrapper<OrderVO> wrapper = new QueryWrapper<>();
//        wrapper.like("t2.name", "三");
        System.err.println(wrapper.getCustomSqlSegment());
        Page<OrderVO> orderInfo = orderMapper.getOrderInfo(page, wrapper);
        return orderInfo;
    }
}

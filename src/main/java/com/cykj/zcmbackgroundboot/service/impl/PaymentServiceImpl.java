package com.cykj.zcmbackgroundboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.mapper.PaymentMapper;
import com.cykj.zcmbackgroundboot.pojo.Payment;
import com.cykj.zcmbackgroundboot.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentMapper paymentMapper;
    @Override
    public Page<Payment> getPaymentInfo(int pageNum, int pageSize) {
        Page<Payment> page = new Page<>(pageNum, pageSize);
        QueryWrapper<Payment> wrapper = new QueryWrapper<>();
//        wrapper.like("t2.name", "三");
        System.err.println(wrapper.getCustomSqlSegment());
        paymentMapper.selectPage(page,wrapper);
        return page;
    }
}

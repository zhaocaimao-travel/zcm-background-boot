package com.cykj.zcmbackgroundboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cykj.zcmbackgroundboot.dto.PassengerDTO;
import com.cykj.zcmbackgroundboot.mapper.PassengerMapper;
import com.cykj.zcmbackgroundboot.pojo.Passenger;
import com.cykj.zcmbackgroundboot.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author 小城南姝
 * @date 2023-12-26 15:29
 */
@Service
public class PassengerServiceImpl extends ServiceImpl<PassengerMapper, Passenger> implements PassengerService {

    @Autowired
    private PassengerMapper passengerMapper;


    /**
     * 获取乘客信息
     * @param passengerDTO 前端消息参数
     * @return
     */
    @Override
    public Page<Passenger> getPassengerInfo(PassengerDTO passengerDTO) {
        // 创建一个 QueryWrapper 对象
        QueryWrapper<Passenger> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(passengerDTO.getName())) {
            queryWrapper.like("name", passengerDTO.getName());
        }
        if (StringUtils.isNotBlank(passengerDTO.getPhone())) {
            queryWrapper.like("phone", passengerDTO.getPhone());
        }
        if (StringUtils.isNotBlank(passengerDTO.getIdCard())) {
            queryWrapper.like("id_card", passengerDTO.getIdCard());
        }
        Page<Passenger> page = new Page<>(passengerDTO.getCurrentPage(), passengerDTO.getPageSize());  // 第一页，每页显示 10 条记录
        passengerMapper.selectPage(page, queryWrapper);
        return page;
    }

    /**
     * 批量删除和单删除
     * @param passengerId 乘客id数组
     * @return
     */
    @Override
    public int deleteByInfoId(List<Integer> passengerId) {
        return passengerMapper.deleteByInfoId(passengerId);
    }

    /**
     * 重置乘客密码为123456
     * @param passengerId 乘客id
     * @return
     */
    @Override
    public int resetPassengerPwd(int passengerId) {
        return passengerMapper.resetPassengerPwd(passengerId);
    }

    /**
     * 编辑乘客信息
     * @param passengerDTO 封装乘客要编辑信息的dto
     * @return
     */
    @Override
    public int editPassengerInfo(PassengerDTO passengerDTO) {
        return passengerMapper.editPassengerInfo(passengerDTO);
    }
}

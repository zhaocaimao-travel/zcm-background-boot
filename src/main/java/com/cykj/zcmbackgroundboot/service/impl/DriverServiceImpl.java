package com.cykj.zcmbackgroundboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cykj.zcmbackgroundboot.dto.DriverDTO;
import com.cykj.zcmbackgroundboot.mapper.DriverMapper;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.service.DriverService;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DriverServiceImpl extends ServiceImpl<DriverMapper, Driver> implements DriverService {

    @Autowired
    private DriverMapper driverMapper;

    @Override
    public Page<Driver> getDriverInfo(int pageNum,int pageSize) {
        // 创建一个 QueryWrapper 对象
        QueryWrapper<Driver> queryWrapper = new QueryWrapper<>();
        // 或者使用 selectPage 方法进行分页查询
        Page<Driver> page = new Page<>( pageNum, pageSize);  // 按当前页和每页条数查询记录
        driverMapper.selectPage(page, queryWrapper);
        return page;
    }

    @Override
    public Page<Driver> searchDriverInfo(DriverDTO driverDTO) {
        // 创建一个 QueryWrapper 对象
        QueryWrapper<Driver> queryWrapper = new QueryWrapper<>();
        if(StringUtils.checkValNotNull(driverDTO.getDriverId())){
            queryWrapper.like("driver_id",driverDTO.getDriverId());
        }
        if(StringUtils.isNotBlank(driverDTO.getDriverName())){
            queryWrapper.like("name",driverDTO.getDriverName());
        }
        if(StringUtils.isNotBlank(driverDTO.getJobId())){
            queryWrapper.like("job_id",driverDTO.getJobId());
        }
        // 或者使用 selectPage 方法进行分页查询
        Page<Driver> page = new Page<>(driverDTO.getPageNum(), driverDTO.getPageSize());  // 按当前页和每页条数查询记录
        driverMapper.selectPage(page, queryWrapper);
        return page;
    }

    @Override
    public List<ParamVO> getDriverType() {
        return driverMapper.getDriverType();
    }


    @Override
    public int deleteByDriverId(int driverId) {
        return driverMapper.deleteByDriverId(driverId);
    }

    @Override
    public int resetPwd(int driverId) {
        return driverMapper.resetPwd(driverId);
    }

    @Override
    public int updataDriverInfo(Driver driver) {
        return driverMapper.updataDriverInfo(driver);
    }

    @Override
    public int batchDeleteDriver(List<Integer> driverList) {
        return driverMapper.batchDeleteDriver(driverList);
    }

}

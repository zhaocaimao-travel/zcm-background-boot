package com.cykj.zcmbackgroundboot.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 小城南姝
 * @date 2023-12-29 14:34
 */
@Component
public class MessageUtil {

    /**
     * 获取系统的当前时间
     * @return 当前时间
     */
    public static String getCurrTime() {
        //生成创建用户时间
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTime.format(formatter);
    }

    /**
     * 分割时间
     * @param time 时间格式  2020-10-01 20:10:30
     * @return xxx年xxx月xxx日
     */
    public static String splitTime(String time) {
        String str = time.split(" ")[0];
        String[] timeArr = str.split("-");
        return timeArr[0] + "年" + timeArr[1] + "月" + timeArr[2] + "日";
    }
    /**
     * 同意审核
     * @param name 姓名
     * @param applyTime 申请时间
     * @param agreeTime  通过时间
     * @return 同意消息
     */
    public static String agreeApplyMessage(String name, String applyTime, String agreeTime) {
        agreeTime = splitTime(agreeTime);
        applyTime = splitTime(applyTime);
        return name + "同志: 您" + applyTime + "提交的审核资料已于" + agreeTime + "审核通过,欢迎成为招财猫司机";
    }

    /**
     * 拒绝审核消息
     * @param name 姓名
     * @param applyTime 申请时间
     * @param rejectTime  拒绝时间
     * @param reason  拒绝原因
     * @return 拒绝消息
     */
    public static String rejectApplyMessage(String name, String applyTime, String rejectTime,
                                            String reason) {
        rejectTime = splitTime(rejectTime);
        applyTime = splitTime(applyTime);
        return name + "同志: 您" + applyTime + "提交的申请于" + rejectTime + "审核未通过, 原因: " + reason + ",非常抱歉!";
    }


    /**
     * 套房分配成功发送消息给租客
     * @param name   用户名
     * @param communityName  小区名
     * @param roomNumber   套房号
     * @param assignmentTime  分配时间
     * @return 分配消息
     */
    public static String assignmentSuccess(String name, String communityName, String roomNumber,
                                           String assignmentTime) {
        assignmentTime = splitTime(assignmentTime);
        return name + "同志: 您已于" + assignmentTime + "被分配到" + communityName +
                "小区" + roomNumber + "套房,请在7个工作日之内到大厅进行合同签订, 如果1个月内没有签订合同, 视为" +
                "自动放弃申请, 该套房将自动回收 " ;

    }
}

package com.cykj.zcmbackgroundboot.exception;

import com.cykj.zcmbackgroundboot.result.ResultCode;
import lombok.Getter;

/**
 * @Author 小城南姝
 * @Description //TODO
 * @Date 2023/11/25 22:07
 **/
@Getter
public class CustomException extends RuntimeException{
    public CustomException(String msg) {
        super(msg);
    }

    /**
     * 可传参数的自定义异常
     * @param msg   消息字符串
     * @param param  对象数组  参数
     * @return  自定义异常对象
     */
    public static CustomException transform(String msg, Object... param) {
        if (param.length > 0) {
            for (int index = 0; index < param.length; index++) {
                String tag = "{" + index + "}";
                msg = msg.replace(tag, String.valueOf(param[index]));  //替换
            }
        }
        return new CustomException(msg);
    }

}

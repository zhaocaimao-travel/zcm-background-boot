package com.cykj.zcmbackgroundboot.exception;


import com.cykj.zcmbackgroundboot.result.Result;
import com.cykj.zcmbackgroundboot.result.ResultCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author 小城南姝
 * @Description //TODO
 * @Date 2023/11/25 22:07
 **/
@RestControllerAdvice(basePackages = "com.cykj.zcmbackgroundboot.controller")
public class GlobalExceptionHandler {
    @ExceptionHandler(CustomException.class)
    public Result APIExceptionHandler(CustomException e) {
        // log.error(e.getMessage(), e); 由于还没集成日志框架，暂且放着，写上TODO
        return new Result(ResultCode.EXCEPTION_ERROR.getCode(),ResultCode.EXCEPTION_ERROR.getMsg(),null);
    }
    @ExceptionHandler(Exception.class)
    public Result MainExceptionHandler(Exception e){
        return Result.error(ResultCode.EXCEPTION_ERROR,e.getMessage());
    }
}

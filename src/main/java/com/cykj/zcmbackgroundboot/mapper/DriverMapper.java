package com.cykj.zcmbackgroundboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;


@Mapper
@Component
public interface DriverMapper extends BaseMapper<Driver> {
    int deleteByDriverId(@Param("driverId") int driverId);
    int resetPwd(@Param("driverId") int driverId);
    List<ParamVO> getDriverType();
    int updataDriverInfo(Driver driver);
    int batchDeleteDriver(@Param("driverList")List<Integer> list);
}

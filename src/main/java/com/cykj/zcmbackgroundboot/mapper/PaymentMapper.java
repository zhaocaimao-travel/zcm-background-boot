package com.cykj.zcmbackgroundboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cykj.zcmbackgroundboot.pojo.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface PaymentMapper extends BaseMapper<Payment> {

}

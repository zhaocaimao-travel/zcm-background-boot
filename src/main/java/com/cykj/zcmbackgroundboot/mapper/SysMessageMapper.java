package com.cykj.zcmbackgroundboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cykj.zcmbackgroundboot.dto.SysMessageDTO;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.pojo.SysMessage;
import com.cykj.zcmbackgroundboot.vo.ParamVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;


@Mapper
@Component
public interface SysMessageMapper extends BaseMapper<SysMessage> {
    List<ParamVO> getReceiverType();
    List<ParamVO> getSysMessageState();
    List<ParamVO> getSysMessageType();
    int addSysMessage(SysMessageDTO sysMessageDTO);
    int deleteSysMessage(@Param("sysMessageId") int sysMessageId);
    int batchDeleteSysMessage(@Param("sysMessageList")List<Integer> list);
}

package com.cykj.zcmbackgroundboot.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.pojo.Orders;
import com.cykj.zcmbackgroundboot.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.sql.Wrapper;
import java.util.List;

@Component
@Mapper
public interface OrderMapper extends BaseMapper<Orders> {
    /**
     * 假删除订单信息
     * @param orderId   订单id
     * @return  受影响的数据行数
     */
    int deleteByOrderIdInt(@Param("orderId") int orderId);

    /**
     * 批量删除订单信息 - 假删除
     * @param list  订单id列表
     * @return  受影响的数据行数
     */
    int batchDeleteOrder(@Param("orderArray")List<Integer> list);

    Page<OrderVO> getOrderInfo(Page<OrderVO> page, @Param("ew") QueryWrapper<OrderVO> wrapper);


}

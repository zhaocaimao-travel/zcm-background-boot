package com.cykj.zcmbackgroundboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cykj.zcmbackgroundboot.dto.PassengerDTO;
import com.cykj.zcmbackgroundboot.pojo.Passenger;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 小城南姝
 * @date 2023-12-26 15:27
 */
@Mapper
@Component
public interface PassengerMapper extends BaseMapper<Passenger> {

    int deleteByInfoId(@Param("passengerId") List<Integer> passengerId);

    int resetPassengerPwd(@Param("passengerId") int passengerId);

    int editPassengerInfo(PassengerDTO passengerDTO);
}

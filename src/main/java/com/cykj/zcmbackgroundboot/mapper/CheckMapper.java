package com.cykj.zcmbackgroundboot.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cykj.zcmbackgroundboot.dto.CheckDTO;
import com.cykj.zcmbackgroundboot.pojo.Driver;
import com.cykj.zcmbackgroundboot.vo.OrderVO;
import com.cykj.zcmbackgroundboot.vo.ProcessVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface CheckMapper extends BaseMapper<Driver> {

    Page<ProcessVO> checkDriverInfo(Page<ProcessVO> page, @Param("ew") QueryWrapper<ProcessVO> wrapper);

    int upDateCheckState(@Param("processId") Integer processId);
    int insertMessage(CheckDTO checkDTO);

    int insertDriver(CheckDTO checkDTO);

    int updateRefuseCheckState(@Param("processId") Integer processId);

}
